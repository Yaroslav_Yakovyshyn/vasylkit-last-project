window.addEventListener('DOMContentLoaded', () => {
    const serverAdres = "http://192.168.31.35:8080/api/";
    
    if (document.cookie == '') {
        getCookieServer();
    };
    

    let card;
    let cardTovarAll;
    
    let id1;
    let id2;

    async function requestAnimalTypesAndCompany() {
        let response = await fetch(serverAdres + "animalTypes", {
            method: "GET",
            credentials: "include",
        });
        if (!response.ok) {
            console.error('Не прийшла відповідь з серверу на тип тварин');
        };


        const json = await response.json();

        json.forEach(element => {
            const createElementContainerLi = document.createElement('li');
            const createElementContainerLiA = document.createElement('a');
            const createElementContainerLiUl = document.createElement('ul');
            const createElementContainerLiUlLi = document.createElement('li');

            createElementContainerLiA.style.fontWeight = "bold";
            createElementContainerLiA.style.fontSize = "20px";

            createElementContainerLi.classList.add('nav-item');
            createElementContainerLi.classList.add('dropdown');
            createElementContainerLiA.classList.add('nav-link');
            createElementContainerLiA.classList.add('dropdown-toggle');
            createElementContainerLiA.setAttribute('href', '#');
            createElementContainerLiA.id = 'navbarDropdown';
            createElementContainerLiA.setAttribute('role', 'button');
            createElementContainerLiA.setAttribute('data-bs-toggle', 'dropdown');
            createElementContainerLiA.setAttribute('aria-expanded', 'false');
            createElementContainerLiUl.classList.add('dropdown-menu');
            createElementContainerLiUl.setAttribute('aria-labelledby', 'navbarDropdown');

            createElementContainerLiA.innerHTML = element.name;


            createElementContainerLi.appendChild(createElementContainerLiA);
            createElementContainerLi.appendChild(createElementContainerLiUl);
            document.getElementById('dropdownUl').appendChild(createElementContainerLi);

            element.companyList.forEach(companyListElement => {
                createElementContainerLiUl.appendChild(createElementContainerLiUlLi);
                const createElementContainerLiUlLiA = document.createElement('a');
                createElementContainerLiUlLiA.classList.add('dropdown-item');
                createElementContainerLiUlLiA.setAttribute('href', '#');

                createElementContainerLiUlLiA.innerHTML = companyListElement.name;
                createElementContainerLiUlLi.appendChild(createElementContainerLiUlLiA);
                constractCompanyOnClick(createElementContainerLiUlLiA, `type_id=${element.id}&company_id=${companyListElement.id}`);
                id1 = element.id;
                id2 = companyListElement.id;

            });


        });

       
    };
    requestAnimalTypesAndCompany();
   




    
    let numPageUl;
    async function constractNavBarPages(id, totalPages) {

        numPageUl = document.createElement("ul");
        numPageUl.classList.add('pagination');
        numPageUl.classList.add('justify-content-center');
        document.getElementById('pageNum').appendChild(numPageUl);

        for (let i = 0; i < totalPages; i++) {
            let numPageLi = document.createElement('li');
            let numPageLiA = document.createElement('a');
            numPageLiA.classList.add('page-link');
            numPageLiA.setAttribute('href', '#');
            numPageLiA.innerHTML = i + 1;
            numPageLi.classList.add('page-item');
            numPageLi.appendChild(numPageLiA);
            buttonPage(numPageLiA, numPageLi, id);
            numPageUl.appendChild(numPageLi);

        };

    };

    
    let typeId;
    let companyId;

    async function constractTovarList(id, page) {

        let response = await fetch(serverAdres + `goods?page=${page}&` + id, {
            method: "GET",
            credentials: "include",
        });
        if (!response.ok) {
            console.error('Запрос на получение полного списка товара не проходит');
        };
        const json = await response.json();

        if (cardTovarAll != null) {
            cardTovarAll.remove();
        };
        cardTovarAll = document.createElement('div');
        document.getElementById('cardTovar').appendChild(cardTovarAll);
        json.content.forEach(element => {
            card = document.createElement('div');
            card.classList.add('card');
            card.style.width = '16rem';
            card.style.display = 'inline-block';
            card.style.marginLeft = '40px';
            card.style.textAlign = 'center';

            let photoTovar = document.createElement('img');
            let divContainer = document.createElement('div');
            let divContainerH5 = document.createElement('h5');
            let divContainerP = document.createElement('p');
            let divContainerAMoreInformation = document.createElement('a');
            let divContainerABuy = document.createElement('a');

            photoTovar.setAttribute('src', element.photoUrl);
            photoTovar.classList.add('card-img-top');
            photoTovar.setAttribute('alt', 'Фото товару');
            photoTovar.style.width = "100x";
            photoTovar.style.height = "400px"
            divContainer.classList.add('card-body');
            divContainerH5.classList.add('card-title');
            divContainerH5.innerHTML = element.title;
            divContainerP.classList.add('card-text');
            divContainerP.innerHTML = element.shotDescription;
            divContainerAMoreInformation.setAttribute('href', '#');
            divContainerAMoreInformation.style.display = 'block';
            divContainerAMoreInformation.style.marginBottom = '10px';
            divContainerAMoreInformation.innerHTML = "Більше інформації про товар-->";
            divContainerABuy.setAttribute('href', '#');
            divContainerABuy.classList.add('btn');
            divContainerABuy.classList.add('btn-primary');
            divContainerABuy.innerHTML = 'Купити';
            divContainerABuy.setAttribute('type', 'button');
            divContainerABuy.setAttribute('data-bs-toggle', 'modal');
            divContainerABuy.setAttribute('data-bs-target', '#exampleModal');



            card.appendChild(photoTovar);
            card.appendChild(divContainer);
            divContainer.appendChild(divContainerH5);
            divContainer.appendChild(divContainerP);
            divContainer.appendChild(divContainerAMoreInformation);
            divContainer.appendChild(divContainerABuy);

            cardTovarAll.appendChild(card);

            aboutTovarFullButton(divContainerAMoreInformation, element);
            buttonBuy(divContainerABuy, element);



        });

        constractNavBarPages(id, json.totalPages);
    };
    constractTovarList('', 0);
   
    function aboutTovar(element) {
        let aboutTovarDivPhoto = document.createElement('div');
        let aboutTovarDivDescription = document.createElement('div');
        let aboutTovarDivButtonBuy = document.createElement('div');
        aboutTovarDivButtonBuy.classList.add('col-sm');
        aboutTovarDivPhoto.classList.add('col-sm');
        aboutTovarDivDescription.classList.add('col-sm');
        aboutTovarDivDescription.style.width = "300px";

        aboutTovarDivPhoto.innerHTML = `<img src = ${element.photoUrl}>`;

        const buttonMoreBuy = document.createElement('a');
        buttonMoreBuy.setAttribute('href', '#');
        buttonMoreBuy.classList.add('btn');
        buttonMoreBuy.classList.add('btn-primary');
        buttonMoreBuy.innerHTML = 'Купити';
        buttonMoreBuy.setAttribute('type', 'button');
        buttonMoreBuy.setAttribute('data-bs-toggle', 'modal');
        buttonMoreBuy.setAttribute('data-bs-target', '#exampleModal');
        buttonMoreBuy.style.marginTop = "100px";
        aboutTovarDivButtonBuy.appendChild(buttonMoreBuy);


        aboutTovarDivDescription.innerHTML = element.fullDescription;
        document.getElementById('row').appendChild(aboutTovarDivPhoto);
        document.getElementById('row').appendChild(aboutTovarDivButtonBuy);
        document.getElementById('row2').appendChild(aboutTovarDivDescription);

        buttonMoreBuy1(aboutTovarDivButtonBuy, element);




    }
   
    function basket() {
        let inputBasket = document.getElementById('inputBasket');
        inputBasket.addEventListener('input', () => {
            numPageUl.remove();
            constractTovarList("query=" + inputBasket.value, 0);
        });
    };
    basket();
   
    async function getCookieServer() {

        let response = await fetch(serverAdres + "user", {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();

        document.cookie = "user=" + data.token;

    };

  



    let tovarBasketDivContainer;
    let divContainer;


    async function getShoppingCart() {
        let response = await fetch(serverAdres + "user?token=" + document.cookie.substring(5), {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        if (divContainer != null) {
            divContainer.remove();
        }
        divContainer = document.createElement('div');
        divContainer.classList.add('container');
        document.getElementById('modal-body').appendChild(divContainer);

        let divSumPrice = document.getElementById('sumPrice');
        let sumPrice = 0;



        data.shoppingCart.forEach(element => {

            tovarBasketDivContainer = document.createElement('div');
            const hr = document.createElement('hr');
            divContainer.appendChild(tovarBasketDivContainer);
            divContainer.appendChild(hr);
            let tovarBasketDivContainerDiv1 = document.createElement('div');
            let tovarBasketDivContainerDiv2 = document.createElement('div');
            let tovarBasketDivContainerDiv3 = document.createElement('div');
            let tovarBasketDivContainerDiv4 = document.createElement('div');
            let tovarBasketDivContainerDiv5 = document.createElement('div');
            let tovarBasketDivContainerSpanMinus = document.createElement('span');
            let tovarBasketDivContainerSpanPlus = document.createElement('span');
            let tovarBasketDivContainerSpanAmount = document.createElement('span');




            tovarBasketDivContainer.classList.add('row');
            tovarBasketDivContainerDiv1.classList.add('col-sm');
            tovarBasketDivContainerDiv2.classList.add('col-sm');
            tovarBasketDivContainerDiv3.classList.add('col-sm');
            tovarBasketDivContainerDiv4.classList.add('col-sm');
            tovarBasketDivContainerDiv5.classList.add('col-sm');

            tovarBasketDivContainerDiv2.style.width = "20px";
            tovarBasketDivContainerDiv1.style.cursor = "pointer";
            tovarBasketDivContainerSpanPlus.style.cursor = 'pointer';
            tovarBasketDivContainerSpanMinus.style.cursor = 'pointer';



            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv1);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv2);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv3);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv4);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanMinus);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanAmount);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanPlus);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv5);

            tovarBasketDivContainerDiv1.innerHTML = "видалити &#10008;";
            tovarBasketDivContainerDiv2.innerHTML = `<img id = basketPhoto src = "${element.goods.photoUrl}">`;
            tovarBasketDivContainerDiv3.innerHTML = element.goods.title;
            tovarBasketDivContainerSpanMinus.innerHTML = " &#10008; ";
            tovarBasketDivContainerSpanAmount.innerHTML = element.amount;
            tovarBasketDivContainerSpanPlus.innerHTML = " &#10010; ";
            tovarBasketDivContainerDiv5.innerHTML = element.goods.fullPrice + "грн";

            buttonX(tovarBasketDivContainerDiv1, element);
            plusOneTovar(tovarBasketDivContainerSpanPlus, element);
            minusOneTovar(tovarBasketDivContainerSpanMinus, element.amount, element.id);
            sumPrice = sumPrice + Number(element.goods.fullPrice) * Number(element.amount);

        });
        divSumPrice.innerHTML = sumPrice + ' грн';
    };
   
    async function postBasket(element) {
        let obj = {
            goodsId: element.id,
            userToken: document.cookie.substring(5)
        };
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + "shopping", {
            method: 'POST',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        });

        const result = await response.json();
        return result
    };
  
    async function x(id) {
        let response = await fetch(serverAdres + `shopping/${id}`, {
            method: "DELETE",
            credentials: 'include',
        });
    };
   
    async function minusTovar(num, id) {
        let obj = {
            amount: num - 1,
        };
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + "shopping/" + id, {
            method: 'PUT',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        });
    };
   
    let userAdress = document.getElementById('userAdress');
    let userPhone = document.getElementById('userPhone');
    let userName = document.getElementById('userName');



    async function sendOrder() {
        let obj = {
            userToken: document.cookie.substring(5),
            address: userAdress.value,
            phone: userPhone.value,
            name: userName.value,
        };
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + "order", {
            method: 'POST',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        });

        const result = await response.json();
        return result
    };
   


    async function orderHistoryRequest(token) {

        let response = await fetch(serverAdres + "order?token=" + token, {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        const historyTable = document.createElement('table');
        const historyTableHead = document.createElement('thead');
        const historyTableHeadTr = document.createElement('tr');
        const historyTableHeadTh1 = document.createElement('th');
        const historyTableHeadTh2 = document.createElement('th');
        const historyTableHeadTh3 = document.createElement('th');
        const historyTableBody = document.createElement('tbody');
        if (cardTovarAll != null) {
            cardTovarAll.remove();
        }
        cardTovarAll = document.createElement('div');
        document.getElementById('cardTovar').appendChild(cardTovarAll);

        historyTable.classList.add('table');
        historyTableHeadTh1.setAttribute('scope', 'col');
        historyTableHeadTh2.setAttribute('scope', 'col');
        historyTableHeadTh3.setAttribute('scope', 'col');

        cardTovarAll.appendChild(historyTable);
        historyTable.appendChild(historyTableHead);
        historyTable.appendChild(historyTableBody);
        historyTableHead.appendChild(historyTableHeadTr);
        historyTableHeadTr.appendChild(historyTableHeadTh1);
        historyTableHeadTr.appendChild(historyTableHeadTh2);
        historyTableHeadTr.appendChild(historyTableHeadTh3);

        historyTableHeadTh1.innerHTML = "Фото";
        historyTableHeadTh2.innerHTML = "Назва";
        historyTableHeadTh3.innerHTML = "Кнопка";



        data.forEach(element => {
            const div = document.createElement('div');
            div.style.fontSize = "25px";
            div.style.fontWeight = "bold";
            let date = new Date(element.dateTime);


            div.innerHTML = "Дата покупки: " + date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
            historyTableBody.appendChild(div);

            element.shoppingCart.forEach(e => {

                let historyTableBodyTr = document.createElement('tr');
                let historyTableBodyTd1 = document.createElement('td');
                let historyTableBodyTd2 = document.createElement('td');
                let historyTableBodyTd3 = document.createElement('td');
                let historyTableABuy = document.createElement('a');

                historyTableABuy.setAttribute('href', '#');
                historyTableABuy.classList.add('btn');
                historyTableABuy.classList.add('btn-primary');
                historyTableABuy.innerHTML = 'Купити';
                historyTableABuy.setAttribute('type', 'button');
                historyTableABuy.setAttribute('data-bs-toggle', 'modal');
                historyTableABuy.setAttribute('data-bs-target', '#exampleModal');


                historyTableBody.appendChild(historyTableBodyTr);
                historyTableBodyTr.appendChild(historyTableBodyTd1);
                historyTableBodyTr.appendChild(historyTableBodyTd2);
                historyTableBodyTr.appendChild(historyTableBodyTd3);
                historyTableBodyTd3.appendChild(historyTableABuy);

                historyButtonBuy(historyTableABuy, e.goods);


                historyTableBodyTd1.innerHTML = `<img style="width:50px" src="${e.goods.photoUrl}">`;
                historyTableBodyTd2.innerHTML = e.goods.title;

                



            });
        });
    };
   
    function historyButtonBuy(button, element) {
        button.addEventListener('click', () => {

            postBasket(element).then(result => getShoppingCart());
        });
    };
   
    document.getElementById('orderHistory').addEventListener('click', () => {
        orderHistoryRequest(document.cookie.substring(5));
        if (cardTovarAll != null) {
            cardTovarAll.remove();
        };
        numPageUl.remove();
    });

   
    function buttonMoreBuy1(button, element) {
        button.addEventListener('click', () => {
            console.log("click");
            postBasket(element).then(result => getShoppingCart());
        });
    };
   
    document.getElementById('makeOrder').addEventListener('click', () => {
        console.log("замовлення відправлене");
        if (userAdress.value == '' || userName.value == "" || userName.value == "") {
            alert('Заповніть всі поля');
        } else {
            sendOrder().then(resu => getShoppingCart());
            swal("Дякуємо за замовлення", "Вам зателефонує наш опреатор", "success");
        };

    });
    
    function minusOneTovar(button, amount, id) {
        button.addEventListener('click', () => {
            minusTovar(amount, id).then(res => getShoppingCart());

           
        });
    };
   
    function plusOneTovar(button, element) {
        button.addEventListener('click', () => {
            postBasket(element.goods).then(result => getShoppingCart());
        });
    };
    
    function buttonX(button, element) {
        button.addEventListener('click', () => {
            x(element.id).then(result => getShoppingCart());
        });
    };
   
    document.getElementById('buttonBasket').addEventListener('click', () => {
        getShoppingCart();
    })
   
    function buttonBuy(button, element) {
        button.addEventListener('click', () => {
            postBasket(element).then(result => getShoppingCart());


        });
    };
   
    function buttonPage(pageValue, checkbox, id) {
        pageValue.addEventListener('click', () => {
            numPageUl.remove();
            constractTovarList(id, pageValue.innerText - 1);
            
            checkbox.classList.add('active');

        });
    };
   
    function aboutTovarFullButton(button, element) {
        button.addEventListener('click', () => {
            numPageUl.remove();
            if (cardTovarAll != null) {
                cardTovarAll.remove();
            };
            aboutTovar(element);

        });
    };
    function constractCompanyOnClick(elementClick, id) {
        elementClick.addEventListener('click', () => {
            constractTovarList(id, 0);
            numPageUl.remove();
        });
    };
   




});
