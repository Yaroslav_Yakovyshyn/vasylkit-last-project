window.addEventListener('DOMContentLoaded', () => {
    const serverAdres = "http://188.166.164.180:8080/api/";
    // Проверка на наличие куки НАЧАЛО
    if (document.cookie == '') {
        getCookieServer();
    };
    // Проверка на наличие куки Конец

    let card;
    let cardTovarAll;
    // Функції для додавання тварин та виробників ПОЧАТОК
    let id1;
    let id2;

    async function requestAnimalTypesAndCompany() {
        let response = await fetch(serverAdres + "animalTypes", {
            method: "GET",
            credentials: "include",
        })
        if (!response.ok) {
            console.error('Не прийшла відповідь з серверу на тип тварин');
        };


        const json = await response.json();

        json.forEach(element => {
            const createElementContainerLi = document.createElement('li');
            const createElementContainerLiA = document.createElement('a');
            const createElementContainerLiUl = document.createElement('ul');
            const createElementContainerLiUlLi = document.createElement('li');


            createElementContainerLiA.style.fontWeight = "bold";
            createElementContainerLiA.style.fontSize = "20px";

            createElementContainerLi.classList.add('nav-item');
            createElementContainerLi.classList.add('dropdown');
            createElementContainerLiA.classList.add('nav-link');
            createElementContainerLiA.classList.add('dropdown-toggle');
            createElementContainerLiA.setAttribute('href', '#');
            createElementContainerLiA.id = 'navbarDropdown';
            createElementContainerLiA.setAttribute('role', 'button');
            createElementContainerLiA.setAttribute('data-bs-toggle', 'dropdown');
            createElementContainerLiA.setAttribute('aria-expanded', 'false');
            createElementContainerLiUl.classList.add('dropdown-menu');
            createElementContainerLiUl.setAttribute('aria-labelledby', 'navbarDropdown');

            createElementContainerLiA.innerHTML = element.name;


            createElementContainerLi.appendChild(createElementContainerLiA);
            createElementContainerLi.appendChild(createElementContainerLiUl);
            document.getElementById('dropdownUl').appendChild(createElementContainerLi);


            // element.companyList.forEach(companyListElement => {
            //     createElementContainerLiUl.appendChild(createElementContainerLiUlLi);
            //     const createElementContainerLiUlLiA = document.createElement('a');
            //     createElementContainerLiUlLiA.classList.add('dropdown-item');
            //     createElementContainerLiUlLiA.setAttribute('href', '#');

            //     createElementContainerLiUlLiA.innerHTML = companyListElement.name;
            //     createElementContainerLiUlLi.appendChild(createElementContainerLiUlLiA);
            //     constractCompanyOnClick(createElementContainerLiUlLiA, `type_id=${element.id}&company_id=${companyListElement.id}`)
            //     id1 = element.id;
            //     id2 = companyListElement.id;

            // });
            getCompanyList(element.id, createElementContainerLiUl, createElementContainerLiUlLi, element.id);


        });

        // constractNavBarPages("&type_id="+id1+"&company_id="+id2);
    }
    requestAnimalTypesAndCompany();
    // Функції для додавання тварин та виробників КІнець

    //Функція для отримання виробника товару ПОЧАТОК
    async function getCompanyList(id, create1, create2, id1) {



        let response = await fetch(serverAdres + "companies?typeId=" + id, {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        console.log(data);

        data.forEach(companyListElement => {
            create1.appendChild(create2);
            const createElementContainerLiUlLiA = document.createElement('a');
            createElementContainerLiUlLiA.classList.add('dropdown-item');
            createElementContainerLiUlLiA.setAttribute('href', '#');

            createElementContainerLiUlLiA.innerHTML = companyListElement.name;
            create2.appendChild(createElementContainerLiUlLiA);
            constractCompanyOnClick(createElementContainerLiUlLiA, `type_id=${id1}&company_id=${companyListElement.id}`)
            id1 = id1;
            id2 = companyListElement.id;
        });
    };
    //Функція для отримання виробника товару КІНЕЦЬ




    // Функції для додавання тварин та виробників КІНЕЦЬ

    // Функція для отримання кількості сторінок open
    let numPageUl;
    async function constractNavBarPages(id, totalPages) {

        numPageUl = document.createElement("ul");
        numPageUl.classList.add('pagination');
        numPageUl.classList.add('justify-content-center');
        document.getElementById('pageNum').appendChild(numPageUl);

        for (let i = 0; i < totalPages; i++) {
            let numPageLi = document.createElement('li');
            let numPageLiA = document.createElement('a');
            numPageLiA.classList.add('page-link');
            numPageLiA.setAttribute('href', '#');
            numPageLiA.innerHTML = i + 1;
            numPageLi.classList.add('page-item');
            numPageLi.appendChild(numPageLiA);
            buttonPage(numPageLiA, numPageLi, id);
            numPageUl.appendChild(numPageLi);

        }

    }

    // Функція для отримання кількості сторінок close

    // Функція для додавання товару на сторінку початок
    let typeId;
    let companyId;

    async function constractTovarList(id, page) {

        let response = await fetch(serverAdres + `goods?page=${page}&` + id, {
            method: "GET",
            credentials: "include",
        });
        if (!response.ok) {
            console.error('Запрос на получение полного списка товара не проходит');
        }
        const json = await response.json();

        if (cardTovarAll != null) {
            cardTovarAll.remove();
        }
        if (numPageUl != null) {
            numPageUl.remove()
        }
        cardTovarAll = document.createElement('div');
        document.getElementById('cardTovar').appendChild(cardTovarAll);
        json.content.forEach(element => {
            card = document.createElement('div');
            card.classList.add('card');
            card.style.Width = '16rem';

            card.style.display = 'inline-block';
            card.style.marginLeft = '40px';
            card.style.textAlign = 'center';

            let photoTovar = document.createElement('img');
            let divContainer = document.createElement('div');
            let divContainerH5 = document.createElement('h5');
            let divContainerP = document.createElement('p');
            let divContainerAMoreInformation = document.createElement('a');
            let divContainerABuy = document.createElement('a');

            photoTovar.setAttribute('src', element.photoUrl);
            photoTovar.classList.add('card-img-top');
            photoTovar.setAttribute('alt', 'Фото товару');
            photoTovar.style.width = '280px';

            // photoTovar.style.backgroundImage = 'url(' + element.photoUrl  + ')';
            // photoTovar.style.backgroundRepeat = 'no-repeat';
            // photoTovar.style.backgroundSize = 'cover';
            // photoTovar.style.backgroundPositionX = '50%';
            // photoTovar.style.backgroundPositionY = '50%';

            // photoTovar.style.height = "380px";


            divContainer.classList.add('card-body');
            divContainerH5.classList.add('card-title');
            divContainerH5.innerHTML = element.title;
            divContainerP.classList.add('card-text');
            divContainerP.style.maxWidth = '220px';
            divContainerP.style.textAlign = 'center';
            divContainerP.style.whiteSpace = 'nowrap';
            divContainerP.style.overflow = 'hidden';
            divContainerP.style.textOverflow = 'ellipsis';
            divContainerH5.style.maxWidth = '220px';
            divContainerH5.style.textAlign = 'center';
            divContainerH5.style.whiteSpace = 'nowrap';
            divContainerH5.style.overflow = 'hidden';
            divContainerH5.style.textOverflow = 'ellipsis';

            divContainerP.innerHTML = element.shotDescription;
            divContainerAMoreInformation.setAttribute('href', '#');
            divContainerAMoreInformation.style.display = 'block';
            divContainerAMoreInformation.style.marginBottom = '10px';
            divContainerAMoreInformation.innerHTML = "Більше інформації про товар-->"
            divContainerABuy.setAttribute('href', '#');
            divContainerABuy.classList.add('btn');
            divContainerABuy.classList.add('btn-primary');
            divContainerABuy.innerHTML = 'Купити';
            divContainerABuy.setAttribute('type', 'button');
            divContainerABuy.setAttribute('data-bs-toggle', 'modal');
            divContainerABuy.setAttribute('data-bs-target', '#exampleModal');



            card.appendChild(photoTovar);
            card.appendChild(divContainer);
            divContainer.appendChild(divContainerH5);
            divContainer.appendChild(divContainerP);
            divContainer.appendChild(divContainerAMoreInformation);
            divContainer.appendChild(divContainerABuy);

            cardTovarAll.appendChild(card);

            aboutTovarFullButton(divContainerAMoreInformation, element);
            buttonBuy(divContainerABuy, element);



        });

        constractNavBarPages(id, json.totalPages);
    }
    constractTovarList('', 0);
    // Функція для додавання товару на сторінку кінець


    // Функція для перегляду інформації протовар ПОЧАТОК



    let aboutTovarDivPhoto = document.createElement('div');
    let aboutTovarDivDescription1 = document.createElement('div');
    let aboutTovarDivDescription2 = document.createElement('div');
    let aboutTovarDivDescription3 = document.createElement('div');
    let aboutTovarDivButtonBuy = document.createElement('div');
    const buttonMoreBuy = document.createElement('a');

    function aboutTovar(element) {

        aboutTovarDivButtonBuy.classList.add('col-sm');
        aboutTovarDivPhoto.classList.add('col-sm');
        aboutTovarDivDescription1.classList.add('col-sm');
        aboutTovarDivDescription1.style.width = "300px";
        aboutTovarDivDescription2.classList.add('col-sm');
        aboutTovarDivDescription2.style.fontWeight = "bold";
        aboutTovarDivDescription2.style.marginBottom = '50px';
        aboutTovarDivDescription2.style.fontSize = '15  pt';

        aboutTovarDivDescription3.classList.add('col-sm');
        aboutTovarDivDescription3.style.fontWeight = "bold";
        aboutTovarDivDescription3.style.marginBottom = '50px';
        aboutTovarDivDescription3.style.fontSize = '15  pt';


        aboutTovarDivPhoto.innerHTML = `<img src = ${element.photoUrl}>`;

        buttonMoreBuy.setAttribute('href', '#');
        buttonMoreBuy.classList.add('btn');
        buttonMoreBuy.classList.add('btn-primary');
        buttonMoreBuy.innerHTML = 'Купити';
        buttonMoreBuy.setAttribute('type', 'button');
        buttonMoreBuy.setAttribute('data-bs-toggle', 'modal');
        buttonMoreBuy.setAttribute('data-bs-target', '#exampleModal');
        buttonMoreBuy.style.marginTop = "100px";
        aboutTovarDivButtonBuy.appendChild(buttonMoreBuy);


        aboutTovarDivDescription1.innerHTML = element.fullDescription;
        aboutTovarDivDescription2.innerHTML = element.shotDescription;
        aboutTovarDivDescription3.innerHTML = element.title;

        console.log(element);


        document.getElementById('row').appendChild(aboutTovarDivPhoto);
        document.getElementById('row').appendChild(aboutTovarDivButtonBuy);
        document.getElementById('row4').appendChild(aboutTovarDivDescription3)
        document.getElementById('row3').appendChild(aboutTovarDivDescription2);
        document.getElementById('row2').appendChild(aboutTovarDivDescription1);

        buttonMoreBuy1(aboutTovarDivButtonBuy, element);




    }
    // Функція для перегляду інформації протовар кІНЕЦЬ

    // Функція для пошуку ПОЧАТОК
    function basket() {
        let inputBasket = document.getElementById('inputBasket');
        inputBasket.addEventListener('input', () => {


            constractTovarList("query=" + inputBasket.value, 0);
            numPageUl.remove();
            if (qrCode != null){
                qrCode.remove();
            }
        })
    }
    basket();
    // Функція для пошуку КІНЕЦЬ

    // Функція для ОТРИМАННЯ кукі на сервер ПОЧАТОК
    async function getCookieServer() {

        let response = await fetch(serverAdres + "user", {
            method: "POST",
            credentials: 'include',

            headers: {
                "Content-type": "application/json"
            }
        })
        const data = await response.json();

        document.cookie = "user=" + data.token;

    }

    // Функція для ОТРИМАННЯ кукі на сервер КІНЕЦЬ

    // Функція для додавання товару в корзину ПОЧАТОК



    let tovarBasketDivContainer;
    let divContainer;


    async function getShoppingCart() {
        let response = await fetch(serverAdres + "user?token=" + document.cookie.substring(5), {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        if (divContainer != null) {
            divContainer.remove();
        }
        divContainer = document.createElement('div');
        divContainer.classList.add('container');
        document.getElementById('modal-body').appendChild(divContainer);

        let divSumPrice = document.getElementById('sumPrice');
        let sumPrice = 0;



        data.shoppingCart.forEach(element => {

            tovarBasketDivContainer = document.createElement('div');
            const hr = document.createElement('hr');
            divContainer.appendChild(tovarBasketDivContainer);
            divContainer.appendChild(hr);
            let tovarBasketDivContainerDiv1 = document.createElement('div');
            let tovarBasketDivContainerDiv2 = document.createElement('div');
            let tovarBasketDivContainerDiv3 = document.createElement('div');
            let tovarBasketDivContainerDiv4 = document.createElement('div');
            let tovarBasketDivContainerDiv5 = document.createElement('div');
            let tovarBasketDivContainerSpanMinus = document.createElement('span');
            let tovarBasketDivContainerSpanPlus = document.createElement('span');
            let tovarBasketDivContainerSpanAmount = document.createElement('span');




            tovarBasketDivContainer.classList.add('row');
            tovarBasketDivContainerDiv1.classList.add('col-sm');
            tovarBasketDivContainerDiv2.classList.add('col-sm');
            tovarBasketDivContainerDiv3.classList.add('col-sm');
            tovarBasketDivContainerDiv4.classList.add('col-sm');
            tovarBasketDivContainerDiv5.classList.add('col-sm');

            tovarBasketDivContainerDiv2.style.width = "20px";
            tovarBasketDivContainerDiv1.style.cursor = "pointer";
            tovarBasketDivContainerSpanPlus.style.cursor = 'pointer';
            tovarBasketDivContainerSpanMinus.style.cursor = 'pointer';



            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv1);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv2);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv3);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv4);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanMinus);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanAmount);
            tovarBasketDivContainerDiv4.appendChild(tovarBasketDivContainerSpanPlus);
            tovarBasketDivContainer.appendChild(tovarBasketDivContainerDiv5);

            tovarBasketDivContainerDiv1.innerHTML = "видалити &#10008;";
            tovarBasketDivContainerDiv2.innerHTML = `<img id = basketPhoto src = "${element.goods.photoUrl}">`;
            tovarBasketDivContainerDiv3.innerHTML = element.goods.title;
            tovarBasketDivContainerSpanMinus.innerHTML = " &#10008; ";
            tovarBasketDivContainerSpanAmount.innerHTML = element.amount;
            tovarBasketDivContainerSpanPlus.innerHTML = " &#10010; ";
            tovarBasketDivContainerDiv5.innerHTML = element.goods.fullPrice + "грн";

            buttonX(tovarBasketDivContainerDiv1, element);
            plusOneTovar(tovarBasketDivContainerSpanPlus, element);
            minusOneTovar(tovarBasketDivContainerSpanMinus, element.amount, element.id);
            sumPrice = sumPrice + Number(element.goods.fullPrice) * Number(element.amount);

        });
        divSumPrice.innerHTML = sumPrice + ' грн'
    }
    // Функція для додавання товару в корзину КІНЕЦЬ

    // Функція для відправки товарів на сервер які додані до корзини ПОЧАТОК
    async function postBasket(element) {
        let obj = {
            goodsId: element.id,
            userToken: document.cookie.substring(5)
        }
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + "shopping", {
            method: 'POST',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        })

        const result = await response.json();
        return result
    }
    // Функція для відправки товарів на сервер які додані до корзини КІНЕЦЬ


    // Функція для видалення товару з корзинки ПОЧАТОК
    async function x(id) {
        let response = await fetch(serverAdres + `shopping/${document.cookie.substring(5)}/${id}`, {
            method: "DELETE",
            credentials: 'include',
        })
    }
    // Функція для видалення товару з корзинки ПОЧАТОК


    // Функція для ПУТ запиту для віднімання товару з корзини ПОЧАТОК
    async function minusTovar(num, id) {
        let obj = {
            amount: num - 1,
        }
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + `shopping/${document.cookie.substring(5)}/` + id, {
            method: 'PUT',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        })
    }
    // Функція для ПУТ запиту для віднімання товару з корзини ПОЧАТОК

    // Функція для відпраки замовлення на телеграм ПОЧАТОК
    let userAdress = document.getElementById('userAdress');
    let userPhone = document.getElementById('userPhone');
    let userName = document.getElementById('userName');



    async function sendOrder() {
        let obj = {
            userToken: document.cookie.substring(5),
            address: userAdress.value,
            phone: userPhone.value,
            name: userName.value,
        }
        let json = JSON.stringify(obj);

        let response = await fetch(serverAdres + "order", {
            method: 'POST',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        })

        const result = await response.json();
        return result
    };
    // Функція для відпраки замовлення на телеграм КІНЕЦЬ

    // Функція для запиту на історію покупок ПОЧАТОК


    async function orderHistoryRequest(token) {

        let response = await fetch(serverAdres + "order?token=" + token, {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        const historyTable = document.createElement('table');
        const historyTableHead = document.createElement('thead');
        const historyTableHeadTr = document.createElement('tr');
        const historyTableHeadTh1 = document.createElement('th');
        const historyTableHeadTh2 = document.createElement('th');
        const historyTableHeadTh3 = document.createElement('th');
        const historyTableBody = document.createElement('tbody');
        if (cardTovarAll != null) {
            cardTovarAll.remove();
        }
        cardTovarAll = document.createElement('div');
        document.getElementById('cardTovar').appendChild(cardTovarAll);

        historyTable.classList.add('table');
        historyTableHeadTh1.setAttribute('scope', 'col');
        historyTableHeadTh2.setAttribute('scope', 'col');
        historyTableHeadTh3.setAttribute('scope', 'col');

        cardTovarAll.appendChild(historyTable);
        historyTable.appendChild(historyTableHead);
        historyTable.appendChild(historyTableBody);
        historyTableHead.appendChild(historyTableHeadTr);
        historyTableHeadTr.appendChild(historyTableHeadTh1);
        historyTableHeadTr.appendChild(historyTableHeadTh2);
        historyTableHeadTr.appendChild(historyTableHeadTh3);

        historyTableHeadTh1.innerHTML = "Фото";
        historyTableHeadTh2.innerHTML = "Назва";
        historyTableHeadTh3.innerHTML = "Кнопка";



        data.forEach(element => {
            const div = document.createElement('div');
            div.style.fontSize = "25px";
            div.style.fontWeight = "bold";
            let date = new Date(element.dateTime);


            div.innerHTML = "Дата покупки: " + date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
            historyTableBody.appendChild(div);

            element.shoppingCart.forEach(e => {

                let historyTableBodyTr = document.createElement('tr');
                let historyTableBodyTd1 = document.createElement('td');
                let historyTableBodyTd2 = document.createElement('td');
                let historyTableBodyTd3 = document.createElement('td')
                let historyTableABuy = document.createElement('a');

                historyTableABuy.setAttribute('href', '#');
                historyTableABuy.classList.add('btn');
                historyTableABuy.classList.add('btn-primary');
                historyTableABuy.innerHTML = 'Купити';
                historyTableABuy.setAttribute('type', 'button');
                historyTableABuy.setAttribute('data-bs-toggle', 'modal');
                historyTableABuy.setAttribute('data-bs-target', '#exampleModal');


                historyTableBody.appendChild(historyTableBodyTr);
                historyTableBodyTr.appendChild(historyTableBodyTd1);
                historyTableBodyTr.appendChild(historyTableBodyTd2);
                historyTableBodyTr.appendChild(historyTableBodyTd3);
                historyTableBodyTd3.appendChild(historyTableABuy);

                historyButtonBuy(historyTableABuy, e.goods);


                historyTableBodyTd1.innerHTML = `<img style="width:50px" src="${e.goods.photoUrl}">`
                historyTableBodyTd2.innerHTML = e.goods.title;

                // <table class="table">
                //     <thead>
                //         <tr>
                //             <th scope="col">Фото</th>
                //             <th scope="col">Назва</th>
                //             <th scope="col">Кнопка купити</th>
                //         </tr>
                //     </thead>
                //     <tbody>
                //         <tr>
                //             <td>Фото</td>
                //             <td>Назва</td>
                //             <td>Кнопка</td>
                //         </tr>
                //     </tbody>
                // </table>
                // document.getElementById('row') - сюди додавати створені елементи




            })
        });
    }
    // Функція для запиту на історію покупок КІНЕЦЬ


    // Фукція для додавання QR коду на сторінку ПОЧАТОК
    let qrCode = document.createElement('img');
    qrCode.style.width = '20%'
    function generationQRCode() {
        let qrCodepaParagrph = document.createElement('p');
        qrCodepaParagrph.style.textAlign = 'center';
        qrCode.setAttribute('src', 'https://i.ibb.co/nBkV0GL/QR.png');
        qrCodepaParagrph.appendChild(qrCode);

        document.getElementById('QrCodePage').appendChild(qrCodepaParagrph);
    }
    // Фукція для додавання QR коду на сторінку КІнець



    // КНОПКИ (


    // Кнопка покупки з історії покупок ПОЧАТОК
    function historyButtonBuy(button, element) {
        button.addEventListener('click', () => {

            postBasket(element).then(result => getShoppingCart());
        })
    }
    // Кнопка покупки з історії покупок КІНЕЦЬ


    // Кнопка історії покупок ПОЧАТОК
    document.getElementById('orderHistory').addEventListener('click', () => {
        orderHistoryRequest(document.cookie.substring(5));
        if (cardTovarAll != null) {
            cardTovarAll.remove();
        }
        numPageUl.remove();
    })

    // Кнопка історії покупок КІНЕЦЬ

    // Кнопка для додавання товару в корзину у більше інформації про товар ПОЧАТОК
    function buttonMoreBuy1(button, element) {
        button.addEventListener('click', () => {
            console.log("click")
            postBasket(element).then(result => getShoppingCart());
        })
    }
    // Кнопка для додавання товару в корзину у більше інформації про товар ПОЧАТОК

    // Кнопка для відпраки замовлення з корзини ПОЧАТОК
    document.getElementById('makeOrder').addEventListener('click', () => {
        console.log("замовлення відправлене");
        if (userAdress.value == '' || userName.value == "" || userName.value == "") {
            alert('Заповніть всі поля');
        } else {
            sendOrder().then(resu => getShoppingCart());
            swal("Дякуємо за замовлення", "Вам зателефонує наш опреатор", "success");
        }

    })
    // Кнопка для відпраки замовлення з корзини КІНЕЦЬ

    // Кнопка для відніманні товару з корзини ПОЧАТОК
    function minusOneTovar(button, amount, id) {
        button.addEventListener('click', () => {
            minusTovar(amount, id).then(res => getShoppingCart());

            console.log(amount);
        });
    }
    // Кнопка для відніманні товару з корзини КІНЕЦЬ

    // Кнопка для додавання одної позиції товару ПОЧАТОК
    function plusOneTovar(button, element) {
        button.addEventListener('click', () => {
            postBasket(element.goods).then(result => getShoppingCart());
        })
    }
    // Кнопка для додавання одної позиції товару КІНЕЦЬ


    // Кнопка "Х" в корзині ПОЧАТОК
    function buttonX(button, element) {
        button.addEventListener('click', () => {
            x(element.id).then(result => getShoppingCart());
        })
    }
    // Кнопка "Х" в корзині КІНЕЦЬ


    // Кнопка корзини в навігації ПОЧАТОК
    document.getElementById('buttonBasket').addEventListener('click', () => {
        getShoppingCart();
    })
    // Кнопка корзини в навігації Кінець
    // Кнопка для додавання товару в корзину ПОЧАТОК
    function buttonBuy(button, element) {
        button.addEventListener('click', () => {
            postBasket(element).then(result => getShoppingCart());


        })
    }
    // Кнопка для додавання товару в корзину КІНЕЦЬ
    // Кнопка для зміни номеру сторінки ПОЧАТОК
    function buttonPage(pageValue, checkbox, id) {
        pageValue.addEventListener('click', () => {
            numPageUl.remove();
            constractTovarList(id, pageValue.innerText - 1);
            // if(checkbox.classList.contains('active') == true){
            //     checkbox.classList.remove()
            // }
            checkbox.classList.add('active');
            // numPageLi.classList.add('active');

        })
    }
    // Кнопка для зміни номеру сторінки КІНЕЦЬ

    // Кнопка для перегляду більше інформації про товар початок
    function aboutTovarFullButton(button, element) {
        button.addEventListener('click', () => {
            numPageUl.remove();
            if (cardTovarAll != null) {
                cardTovarAll.remove();
            }
            aboutTovar(element);

        })
    }
    // Кнопка для відбору товару по фірмам Початок
    function constractCompanyOnClick(elementClick, id) {
        elementClick.addEventListener('click', () => {
            constractTovarList(id, 0);
            if (cardTovarAll != null) {
                cardTovarAll.remove();
            }
            numPageUl.remove();
            if (aboutTovarDivPhoto != null) {
                aboutTovarDivPhoto.remove();
            }
            if (aboutTovarDivDescription1 != null) {
                aboutTovarDivDescription1.remove();
            }
            if (aboutTovarDivDescription2 != null) {
                aboutTovarDivDescription2.remove();
            }
            if (aboutTovarDivDescription3 != null) {
                aboutTovarDivDescription3.remove();
            }
            if (aboutTovarDivButtonBuy != null) {
                aboutTovarDivButtonBuy.remove();
            }
            if (qrCode != null){
                qrCode.remove();
            }
        })
    };

    // Кнопка для отримання QR коду в корзині ПОЧАТОК
    document.getElementById('qrCode').addEventListener('click', () => {
        if (cardTovarAll != null) {
            cardTovarAll.remove();
            numPageUl.remove();

        }
        generationQRCode();

    })
    // Кнопка для отримання QR коду в корзині КІНЕЦЬ

    // КНОПКИ )



})
